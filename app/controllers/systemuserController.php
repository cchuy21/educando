<?php
class systemuserController extends controllerBase {

    public function index() {
        if( authDriver::isLoggedin() ) {
            $this->getView()->controlPanel();
        } else {
            $this->getView()->index();
        }
    }
    
		
}
