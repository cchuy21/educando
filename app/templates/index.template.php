<?php
//session_start();
?><!DOCTYPE HTML>
<html style='background: #f7f7f7;'> <!--archivo principal del servicio, se mandan a llamar los estilos css y js-->
    <head><meta charset="gb18030">
        
        <title>Sistema</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <link href="/static/css/style.css" rel="stylesheet">

    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <?php
                //se inicia sesión para mostrar el menu 
                    if( authDriver::isLoggedin())  {
                        templateDriver::renderSection('systemuser.menu');
                    }
 
                    //templateDriver::content();
                    
                ?>
                asdasd
                <footer>
                    <div class="pull-right">
                        asdads
                    </div>
                    <div class="clearfix"></div>
                </footer>
            </div>
        </div>
  </body>

    <!-- Latest compiled and minified JavaScript -->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/static/js/app.js"></script>  

  </html>