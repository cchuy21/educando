<!--MENU DE INICIO DE SESIÓN PARA LOS USUARIOS-->
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
    
    </div>
</div>
<div class="top_nav">

</div>
<div class="right_col" role="main">
    <div class="row">
        <div class="container">
            <div class="col-md-4" style="margin:0 auto 0;float: revert;">
                <form class="form-signin" role="form" action="/systemuser/login" method="post">
                    <h2 class="form-signin-heading">Iniciar sesión</h2>
                    <br />
                    <center><img class="logo" src="static/images/logo3.jpg" width="90%"></center>
                    <br />
                    <input class="form-control" placeholder="Nombre de Usuario" required="" autofocus="" type="text" name="user[username]">
                    <input class="form-control" placeholder="Contraseña" required="" type="password" name="user[password]">
                    <div style="display: none;" class="alert alert-danger">
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar</button>
                    <a>Olvidé mi contraseña</a>
                </form>
            </div>
        </div> <!-- /container -->
    </div> 
</div> 
