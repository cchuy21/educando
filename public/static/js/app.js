'(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

function getParent(ob, cl) {
	do {
		ob = ob.parent();
	} while (!ob.hasClass(cl));
	return ob;
}
var ed=0;
var arrayc=[];
function padre(element, clase) {
	var tr = false;
	while (!tr) {
		if (element.parent().hasClass(clase)) {
			tr = true;
			return element.parent();
			break;
		} else {
			element = element.parent();
		}
	}
}
/*manda los mensajes cuando se realiza una acción en un botón especifico*/
/*--------------------Objects-----------------------*/
var app = {
	'load' : function(url, container2, options, callback) {
		var container = jQuery(container2);

		if (jQuery.isFunction(options)) {
			callback = options;
		} else {
			if (jQuery.isFunction(options.done)) {
				callback = options.done;
			}
			if (jQuery.isFunction(options.before)) {
				options.before();
			}
		}
		jQuery.ajax({
			type : 'POST',
			dataType : 'html',
			url : url
		}).done(function(d) {
			container.html(d);
			callback();
		});
	}
};
//Funcion generica para autocomplete, todos deben tener el elemente enseguida
//Cambiar esto a un plugin para que se corrija
jQuery(document).ready(function(){
	initFormLogin();
});
function initFormLogin() {
	jQuery('.form-signin').fadeTo(1, 1);
		//inituser
		if (!jQuery(".form-signin").length) {
            //code
			inituser();
			//inituserpanel();
			//inituserbuttons();
        }
		
 	jQuery(".form-signin .btn-block").on("click",function(){
		//obtiene el valor del nombre de usuario que escribieron
		var nm = jQuery("input[name='user[username]']").val();
		var pass = jQuery("input[name='user[password]']").val();
		//manda los datos por el metodo post(ocultos)
		jQuery.ajax({
			url : "/systemuser/login/",
			data : {
				user:nm,
				pass:pass
				},
			type : "POST",
			dataType : "json",
			success : function(source) {				
					//te manda a la página principal del sitio
					window.location = "/index.php";
				}
			
		});

	});
	jQuery('.form-signin').fadeIn(function() {
		//manda el mensaje en consola del navegador
		console.log("Formulario de inicio de sesión cargado.");
	});
}
